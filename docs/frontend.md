# API
Frontend API documentation is available at the [/apidocs](https://lecture-translator.kit.edu/apidocs/) endpoint (created with OpenAPI/Swagger).
